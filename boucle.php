// while

<?php
$nombre_de_lignes = 1;
while ($nombre_de_lignes <= 100)
{
    echo 'Je ne dois pas regarder les mouches voler.<br />';
    $nombre_de_lignes++; // $nombre_de_lignes = $nombre_de_lignes + 1
}
?>

// boucle for

<?php
for ($nombre_de_lignes = 1; $nombre_de_lignes <= 100; $nombre_de_lignes++)
{
    echo 'Ceci est la ligne n°' . $nombre_de_lignes . '<br />';
}
?>
